<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Game extends CI_Controller {
	public function __construct()
	{
		 parent::__construct();
		 $this->load->helper(array('form', 'url'));
	     $this->load->library('form_validation');
		 $this->load->library('session');
	 }

	
	public function index()
	{
		$fname=$this->session->userdata('email');
		$arr = explode("@", $fname, 2);
		$data['name'] = $arr[0];
		$this->load->view('commons/header',$data);
		$this->load->view('home');
		$this->load->view('commons/footer');
	}
	
	public function play()
	{
		if(($this->session->userdata('user_id')!=""))
		{	
			$fname=$this->session->userdata('email');
			$arr = explode("@", $fname, 2);
			$data['name'] = $arr[0];
			//print_r($data);
			$this->load->view('commons/header',$data);
			$this->load->view('gamedetails');
			$this->load->view('commons/footer'); 
		}
		else
		{
			$this->load->view('commons/header');
			$this->load->view('login');
			$this->load->view('commons/footer');
		}
	}
	
	
	
	
}

