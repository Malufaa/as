<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {
	
	public function __construct()
	{
		 parent::__construct();
		 $this->load->model('auth_model');
		 $this->load->helper(array('form', 'url'));
	     $this->load->library('form_validation');
		 $this->load->library('session');
		
	 }

	public function index()
	{
		if(($this->session->userdata('user_id')!=""))
		{
			redirect('/game', 'refresh');
		}
		else
		{   $this->load->view('commons/header');
			$arr['id']=null;
			$this->load->view('login',$arr);
			$this->load->view('commons/footer');
		}		
	}
	
	public function index_one($data)
	{
		if(($this->session->userdata('user_id')!=""))
		{
			redirect('/game', 'refresh');
		}
		else
		{
			$this->load->view('commons/header');
			$this->load->view('login',$data);
			$this->load->view('commons/footer');
		}		
	}
	
	public function login()
	{
		$username=$this->input->post('email');
		$password=md5($this->input->post('password'));	 
	  	$result=$this->auth_model->login($username,$password);
	  	
	  	if($result)
		{
			redirect('/game', 'refresh');
		}
		 else    
		{
			$data['message']="Incorrect Email or password";
			$this->index_one($data);
		}
	}
	
	
	public function signup()
	{
		$username=$this->input->post('email');
		$fname=$this->session->userdata('email');
		$arr = explode("@", $fname, 2);
		$name = $arr[0];
		$password=$this->input->post('password');
		
		$signup=array(
			 'username'			=>		$username,
			 'password'			=> 		md5($password),
			 'first_name'		=>		$name
			 );
			 
		if($this->auth_model->signup($signup))
		{
			$data['signupmessage']="You have successfully registered. Please login to continue!!";
			$this->index_one($data);
		}
		else
		{
			$data['signupmessage']="Your registration was unsuccessful. Please signup again!!";
			$this->index_one($data);
		}
		
	}
	
	public function logout()
	 {
	  $newdata = array(
	  'user_id'  	 => '',
	  'email' 		 => '',
    //'first_name'   => '',
	  'logged_in' 	 => FALSE
	  );
	  $this->session->unset_userdata($newdata );
	  $this->session->sess_destroy();
	  redirect('/game', 'refresh');
	  
	 }
	 
	
	
}

