<html HTML>
    <head>
        <style>
            .courseTable {
	margin:0px;padding:0px;
	width:100%;
	border:1px solid #000000;
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
	
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
	
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
	
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}.courseTable table{
    border-collapse: collapse;
        border-spacing: 0;
	width:100%;
	height:100%;
	margin:0px;padding:0px;
}.courseTable tr:last-child td:last-child {
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
}
.courseTable table tr:first-child td:first-child {
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}
.courseTable table tr:first-child td:last-child {
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
}.courseTable tr:last-child td:first-child{
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
}.courseTable tr:hover td{
	background-color:#e5e5e5;
		

}
.courseTable td{
	vertical-align:middle;
	
	background-color:#ffffff;

	border:1px solid #000000;
	border-width:0px 1px 1px 0px;
	text-align:left;
	padding:8px;
	font-size:15px;
	font-family:Arial;
	font-weight:normal;
	color:#000000;
}
.courseTable tr:last-child td{
	border-width:0px 1px 0px 0px;
}.courseTable tr td:last-child{
	border-width:0px 0px 1px 0px;
}.courseTable tr:last-child td:last-child{
	border-width:0px 0px 0px 0px;
}
.courseTable tr:first-child td{
		background:-o-linear-gradient(bottom, #7f7f7f 5%, #5f5f5f 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #7f7f7f), color-stop(1, #5f5f5f) );
	background:-moz-linear-gradient( center top, #7f7f7f 5%, #5f5f5f 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#7f7f7f", endColorstr="#5f5f5f");	background: -o-linear-gradient(top,#7f7f7f,5f5f5f);

	background-color:#7f7f7f;
	border:0px solid #000000;
	text-align:left;
	border-width:0px 0px 1px 1px;
	font-size:18px;
	font-family:Arial;
	font-weight:bold;
	color:#ffffff;
}
.courseTable tr:first-child:hover td{
	background:-o-linear-gradient(bottom, #7f7f7f 5%, #5f5f5f 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #7f7f7f), color-stop(1, #5f5f5f) );
	background:-moz-linear-gradient( center top, #7f7f7f 5%, #5f5f5f 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#7f7f7f", endColorstr="#5f5f5f");	background: -o-linear-gradient(top,#7f7f7f,5f5f5f);

	background-color:#7f7f7f;
}
.courseTable tr:first-child td:first-child{
	border-width:0px 0px 1px 0px;
}
.courseTable tr:first-child td:last-child{
	border-width:0px 0px 1px 1px;
}
        </style>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>CSE Department</title>
		<link href="style.css" rel="stylesheet" type="text/css">
		  <link href="main.css" rel="stylesheet" type="text/css">

	</head>

<body>
<header>
		   	    <div id = "container">
  <div align="center"><a href="http://www.mec.ac.in"><img src="i.png" width="100" height="100" alt=""/></a></div>
      <h2>DEPARTMENT OF COMPUTER ENGINEERING</h2>
      <h3>GOVT. MODEL ENGINEERING COLLEGE, THRIKKAKARA</h3>
    			</div>
    		  
		
      <ul id="nav">
      <li ><a href= "2.php">Home</a></li>
     
      <li><a href= "#">Faculty</a>
      <ul>
      <li><a href="faculty.php">Profile</a></li>
      <li><a href ="availability.php">Availability</a></li>
      </ul>
      </li>
       <li><a href= "#">Academics</a>
       <ul>
      <li><a href="coursebtech.html"> BTech</a></li>
      <li><a href ="coursemtech.html">MTech</a></li>
      </ul></li>
      <li><a href= "#">Student Corner</a>
      <ul>
      <li><a href="classformtem.php"> BTech</a></li>
      <li><a href ="studentcorner1v.html">MTech</a></li>
      </ul>
      </li>
      <li><a href = "http://www.mec.ac.in/mec/placement-stats.php">Placements</a></li>
      <li><a href = "activities.html">Activites</a></li>
      
      <li><a href = "achievements.php">Achievements</a></li>
      
       <li><a href= "contactus.html">Contact us</a></li>
      </ul>
    </header>


<?php
if(!isset($_GET['sno']))
{header('Location:faculty.php');
}
else
{
$id=$_GET['sno'];
unset($_GET['sno']);
$conn=mysqli_connect("localhost", "root", "","website");
$sql="SELECT * FROM `faculty_profile` WHERE `sno` =$id";
$result = mysqli_query($conn,$sql);
$count = mysqli_num_rows($result);
if($count==0)
{
    mysqli_close($conn);
    header('Location:faculty.php');
}
echo "<br><center><h3>FACULTY PROFILE</h3></center><br>";
$restab = mysqli_fetch_assoc($result);
        
    
echo "<table class='courseTable'><tr><td>Name</td><td>".$restab['fname'].' '.$restab['lname']."</td></tr><tr><td>Designation</td><td>".$restab['designation']."</td></tr><tr><td>Qualification</td><td>".$restab['qualification']."</td></tr>";
    
if($restab['experience']!='')
{
echo "<tr><td>Experience</td><td>".$restab['experience']."</td></tr>";
}
echo "<tr><td>Areas of interest</td><td>".$restab['areasofinterest']."</td></tr><tr><td>Email</td><td>".$restab['email']."</td></tr><tr><td>Website</td><td>".$restab['website']."</td></tr><tr><td>Contact No</td><td>".$restab['contactno']."</td></tr></table>";
}
echo "<br><br>";

$sql1="SELECT * FROM `course_responsibility`,`faculty_profile` WHERE `course_responsibility`.`fid` = `faculty_profile`.`fid` AND `sno` = $id";
$result1 = mysqli_query($conn,$sql1) or die(mysqli_err);
$count1 = mysqli_num_rows($result1);
if($count1!=0)
{
$restab = mysqli_fetch_assoc($result1);
echo "<table class='courseTable'><tr><td>Course/Responsibility</td><td>Class</td></tr>";

if($restab['staffadvisor']==1)
{
        echo"<tr><td>Staff Advisor</td><td>".$restab['class'].'</a>'."</td></tr>";
}

for($i=1;$i<5;$i+=1)
{
    if($restab['subject'.$i]!='')
    {echo "<tr><td>".$restab['subject'.$i]."</td><td>".$restab['class'.$i]."</td></tr>";
     }
}
echo "</table>";
}



mysqli_close($conn);
?>
<br/><br/><br/><br/><br/><br/><br/><br/>p    
    
<div id="footer" align="center" style = "font-size:12px" >
Model Engineering College, Thrikkakara, Kochi, Kerala 682021<br />Webserver Administration : Dept. of Computer Engineering 
 
</div>    
</body>
</html>
