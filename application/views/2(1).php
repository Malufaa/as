<?php
session_start();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>CSE Department</title>
		<link href="style.css" rel="stylesheet" type="text/css">
        <link href="button.css" rel="stylesheet" type="text/css">
        <link href="1.css" rel="stylesheet" type="text/css">
<script>
function login()
{
	window.location.replace("login2.php");
}
</script>
<script>
function logout()
{
	window.location.replace("logout.php");
}
</script>
<script>
function an()
{
	window.location.replace("an.php");
}
</script>


	</head>
	<body> <header>
		   	    <div id = "container">
  <div align="center"><img src="i.png" width="100" height="100" alt=""/> </div>
      <h2>DEPARTMENT OF COMPUTER ENGINEERING</h2>
      <h3>GOVT. MODEL ENGINEERING COLLEGE, THRIKKAKARA</h3>
    			</div>
    		  
		
      <ul id="nav">
      <li ><a href= "2.php">Home</a></li>
     
      <li><a href= "#">Faculty</a>
      <ul>
      <li><a href="faculty.php">Profile</a></li>
      <li><a href ="availability.php">Availability</a></li>
      </ul>
      </li>
       <li><a href= "#">Academics</a>
       <ul>
      <li><a href="coursebtech.html"> BTech</a></li>
      <li><a href ="coursemtech.html">MTech</a></li>
      </ul></li>
      <li><a href= "#">Student Corner</a>
      <ul>
      <li><a href="classformtem.php"> BTech</a></li>
      <li><a href ="studentcorner1v.html">MTech</a></li>
      </ul>
      </li>
      <li><a href = "http://www.mec.ac.in/mec/placement-stats.php">Placements</a></li>
      <li><a href = "activities.html">Activites</a></li>
      
      <li><a href = "achievements.php">Achievements</a></li>
      
       <li><a href= "contactus.html">Contact us</a></li>
      </ul>
    </header>
		
		  <div id="main">
            <div class="wrap">
 
                <div id="primary">
                    <div id="content">
                        <br><br><br><table width="225" height="129" border="1">
  <tr>
    <td width="225" height="123"><h4> Quick links</h4>
         	 
  	<ul>
  		<li><a href="http://www.mec.ac.in">MEC Home</a></li>
  		<li><a href="http://www.mec.ac.in/sessional-marks/">Sessionals</a></li>
  		<li><a href="http://www.mec.ac.in/attn/">Attendance</a></li>
        <li style="list-style: square"><a href="sitemap.html">Site Map</a></li>
  	</ul></td>
 
  </tr>
</table><br><br>
<p><strong>Calendar</strong></p>

<script language="javascript" type="text/javascript">
var day_of_week = new Array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
var month_of_year = new Array('January','February','March','April','May','June','July','August','September','October','November','December');

//  DECLARE AND INITIALIZE VARIABLES
var Calendar = new Date();

var year = Calendar.getFullYear();     // Returns year
var month = Calendar.getMonth();    // Returns month (0-11)
var today = Calendar.getDate();    // Returns day (1-31)
var weekday = Calendar.getDay();    // Returns day (1-31)

var DAYS_OF_WEEK = 7;    // "constant" for number of days in a week
var DAYS_OF_MONTH = 31;    // "constant" for number of days in a month
var cal;    // Used for printing

Calendar.setDate(1);    // Start the calendar day at '1'
Calendar.setMonth(month);    // Start the calendar month at now


/* VARIABLES FOR FORMATTING
NOTE: You can format the 'BORDER', 'BGCOLOR', 'CELLPADDING', 'BORDERCOLOR'
      tags to customize your caledanr's look. */

var TR_start = '<TR>';
var TR_end = '</TR>';
var highlight_start = '<TD WIDTH="30"><TABLE CELLSPACING=0 BORDER=1 BGCOLOR=DEDEFF BORDERCOLOR=CCCCCC><TR><TD WIDTH=20><B><CENTER>';
var highlight_end   = '</CENTER></TD></TR></TABLE></B>';
var TD_start = '<TD WIDTH="30"><CENTER>';
var TD_end = '</CENTER></TD>';

/* BEGIN CODE FOR CALENDAR
NOTE: You can format the 'BORDER', 'BGCOLOR', 'CELLPADDING', 'BORDERCOLOR'
tags to customize your calendar's look.*/

cal =  '<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=0 BORDERCOLOR=BBBBBB><TR><TD>';
cal += '<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=2>' + TR_start;
cal += '<TD COLSPAN="' + DAYS_OF_WEEK + '" BGCOLOR="#EFEFEF"><CENTER><B>';
cal += month_of_year[month]  + '   ' + year + '</B>' + TD_end + TR_end;
cal += TR_start;

//   DO NOT EDIT BELOW THIS POINT  //

// LOOPS FOR EACH DAY OF WEEK
for(index=0; index < DAYS_OF_WEEK; index++)
{

// BOLD TODAY'S DAY OF WEEK
if(weekday == index)
cal += TD_start + '<B>' + day_of_week[index] + '</B>' + TD_end;

// PRINTS DAY
else
cal += TD_start + day_of_week[index] + TD_end;
}

cal += TD_end + TR_end;
cal += TR_start;

// FILL IN BLANK GAPS UNTIL TODAY'S DAY
for(index=0; index < Calendar.getDay(); index++)
cal += TD_start + '  ' + TD_end;

// LOOPS FOR EACH DAY IN CALENDAR
for(index=0; index < DAYS_OF_MONTH; index++)
{
if( Calendar.getDate() > index )
{
  // RETURNS THE NEXT DAY TO PRINT
  week_day =Calendar.getDay();

  // START NEW ROW FOR FIRST DAY OF WEEK
  if(week_day == 0)
  cal += TR_start;

  if(week_day != DAYS_OF_WEEK)
  {

  // SET VARIABLE INSIDE LOOP FOR INCREMENTING PURPOSES
  var day  = Calendar.getDate();

  // HIGHLIGHT TODAY'S DATE
  if( today==Calendar.getDate() )
  cal += highlight_start + day + highlight_end + TD_end;

  // PRINTS DAY
  else
  cal += TD_start + day + TD_end;
  }

  // END ROW FOR LAST DAY OF WEEK
  if(week_day == DAYS_OF_WEEK)
  cal += TR_end;
  }

  // INCREMENTS UNTIL END OF THE MONTH
  Calendar.setDate(Calendar.getDate()+1);

}// end for loop

cal += '</TD></TR></TABLE></TABLE>';

//  PRINT CALENDAR
document.write(cal);

//  End -->
</script>

<br/>



                    </div><!--#content-->
                </div><!--#primary-->
 
                <div id="secondary">
                    <div class="widget">
                    <article><br><br> <br><div><img src="college_1.jpg" width = "650" alt=""/></div>
<h4> About Us</h4>

<p>The foremost objective of the Computer Science and Engineering programme is to hone technical skills demanded by today's engineering professionals by providing a sound technical platform and the required knowledge base. It provides specialization in a wide range of hardware and software subjects in order to bridge the gap between knowledge and its practical application. Thus, the course produces an ideal software engineer, well equipped to meet the challenges faced by today's IT professional by exposing them to a wide array of cutting-edge technologies.</p>
<p>
The Undergraduate Computer Science and Engineering programme of Model Engineering College is ranked among the best technical programs in the country. The department offers a four year course at the end of which you receive a Bachelor of Technology degree in Computer Science and Engineering from Cochin University of Science and Technology.</p>

<p>The program imparts an outstanding educational opportunity for those planning to pursue a career or to gain in-depth knowledge in computing technology and research. Keeping in pace with the IT era, the Computer Engineering syllabus covers an exhaustive realm of core fields like Operating Systems, Compiler Design, Finite Automata Theory, and Computer Graphics, etc.</p>
<p>
The Computer Labs are distinguished along the lines of the Platforms and Operating Systems used. There is a general Computing Lab for beginners, a UNIX Lab with scores of terminals and finally an Advanced Computing Lab that houses some of the best Hardware and software in the Industry.</p>
<h4>Mission</h4>
  <p>  To provide every student with a strong conceptual foundation in the core areas of Computer Science.
    To enable students to engineer ingenious computing hardware and software solutions with the greatest degree of accuracy, flexibility and based on the latest engineering paradigms.
    To familiarize students with the latest in software and programming tools.
    To provide the greatest amount of pragmatism in approaching an engineering solution.
    To provide students with hands-on engineering experience through laboratory courses and project.
    To coach students to work in teams and provide insight and direction into the atmosphere as might be encountered by professionals in the field.</p>

</article>
                    </div><!--.widget-->
                </div><!--#secondary-->
 
                <div id="tertiary">
                    <div class="widget">
                    	<br><br><br>
       

	 
<?php
if(!isset($_SESSION['login_user']))
{
	
 // Closing Connection
?>
<input id="button" type="submit" value="LOGIN" onclick=login() />

<?php 
}
else
{ 
?>	
<b id="welcome">Welcome :<?php if(!strcmp($_SESSION['login_user'],"admin")) {?><a href="meclogo.php"><?php }else {?><a href="meclogo1.php"><?php }  echo $_SESSION['login_user']?></a><?php echo " | "?> <i></i></b>
<b id="Logout"> <a href="logout.php">Logout</a><i></i></b>

<?php }
?>
<div>

<td width="225" height="123"> 
  <caption>
   <h3> <strong>Announcements</strong></h3>
  </caption>
</td>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "website";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
?>

  <?php
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT name
FROM name
ORDER BY id DESC
LIMIT 5;";
$result = $conn->query($sql);
$row=$result->fetch_assoc();
if ($result->num_rows > 0) {
    // output data of each row
    do {
        echo "<li> " . $row["name"]. "</li><br>";
    }while($row = $result->fetch_assoc());
}
 else {
    echo "0 results";
}
$conn->close();
?>
<input id="gobutton" type="submit" value="Read More"  onclick=an()  /> <!--the text in the quotes after value will appear on the button-->

  	</div>				
  		                  </div><!--.widget-->
                </div><!--#tertiary-->
 			
            </div><!--.wrap-->
        </div><!--#main-->
          
    <div id="footer" align="center" style = "font-size:12px" >
Model Engineering College, Thrikkakara, Kochi, Kerala 682021<br />Webserver Administration : Dept. of Computer Engineering
 
   </div>
	</body>
</html>

