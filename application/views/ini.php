<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<body>
	<br>
<?php
session_start();
// Connect to the database
$dbLink = new mysqli('localhost', 'root', '', 'website');
if(mysqli_connect_errno()) 
{
    die("MySQL connection failed: ". mysqli_connect_error());
}
 $class=$_SESSION["class"];
echo "STUDY MATERIALS : " . $class;
// Query for a list of all existing files
$sql = "SELECT `id`, `name`, `description`, `mime`, `size`, `created` FROM `file` WHERE `class`= '" .$class."'";
$result = $dbLink->query($sql);
 
// Check if it was successfull
if($result) {
    // Make sure there are some files in there
    if($result->num_rows == 0) {
        echo '<p>There are no files in the database</p>';
    }
    else {
        // Print the top of a table
        echo '<table width="100%">
                <tr>
                    <td><b>Name</b></td>
                    <td><b>Description</b></td>
                    <td><b>Mime</b></td>
                    <td><b>Size (bytes)</b></td>
                    <td><b>Created</b></td>
                    <td><b>&nbsp;</b></td>
                </tr>';
 
        // Print each file
        while($row = $result->fetch_assoc()) {
            echo "
                <tr>
                    <td>{$row['name']}</td>
                    <td>{$row['description']}</td>
                    <td>{$row['mime']}</td>
                    <td>{$row['size']}</td>
                    <td>{$row['created']}</td>
                    <td><a href='get.php?id={$row['id']}'>Download</a></td>
                </tr>";
        }
 
        // Close table
        echo '</table>';
    }
 
    // Free the result
    $result->free();
}
else
{
    echo 'Error! SQL query failed:';
    echo "<pre>{$dbLink->error}</pre>";
}
 
// Close the mysql connection
$dbLink->close();
?>
        
</div>    
   <br><br><br>     
      <div id="footer" align="center" style = "font-size:12px" >
Model Engineering College, Thrikkakara, Kochi, Kerala 682021<br />Webserver Administration : Dept. of Computer Engineering
 
   </div>
	</body>
</html>